===================
Sale cost Scenario
===================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_chart, \
    ...     get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     create_payment_term


Install sale::

    >>> config = activate_modules(['sale_unit_load', 'sale_cost'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']


Create tax::

    >>> Tax = Model.get('account.tax')
    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()


Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()


Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> address = customer.addresses.new()
    >>> address.name = 'Address 2'
    >>> customer.save()


Uoms::

    >>> ProductUom = Model.get('product.uom')
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> gram, = ProductUom.find([('name', '=', 'Gram')])
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])


Create service product::

    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('50')
    >>> template.cost_price = Decimal('20')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> service = template.products[0]


Create products for sale::

    >>> template = ProductTemplate()
    >>> template.name = 'product 1'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product1 = template.products[0]
    >>> template = ProductTemplate()
    >>> template.name = 'product 2'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product2 = template.products[0]


Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()


Create cases quantity cost types::

    >>> CostType = Model.get('sale.cost.type')
    >>> type_cases = CostType(name='Transport by cases')
    >>> type_cases.product = service
    >>> type_cases.formula = '10.0 * cases_quantity'
    >>> type_cases.distribution_method = 'cases_quantity'
    >>> type_cases.apply_method = 'none'
    >>> type_cases.save()
    >>> type_uls = CostType(name='Transport by ULs')
    >>> type_uls.product = service
    >>> type_uls.formula = '20.0 * ul_quantity'
    >>> type_uls.distribution_method = 'ul_quantity'
    >>> type_uls.apply_method = 'none'
    >>> type_uls.save()


Create cost types with products::

    >>> Product = Model.get('product.product')
    >>> product1 = Product(product1.id)
    >>> product2 = Product(product2.id)
    >>> type_cases2 = CostType(name='Transport by cases')
    >>> type_cases2.product = service
    >>> type_cases2.formula = '10.0 * cases_quantity'
    >>> type_cases2.distribution_method = 'cases_quantity'
    >>> type_cases2.apply_method = 'none'
    >>> type_cases2.products.append(product1)
    >>> type_cases2.products.append(product2)
    >>> type_cases2.save()
    >>> product1 = Product(product1.id)
    >>> product2 = Product(product2.id)
    >>> type_uls2 = CostType(name='Transport by ULs')
    >>> type_uls2.product = service
    >>> type_uls2.formula = '20.0 * ul_quantity'
    >>> type_uls2.distribution_method = 'ul_quantity'
    >>> type_uls2.apply_method = 'none'
    >>> type_uls2.products.append(product1)
    >>> type_uls2.save()


Create cases quantity cost templates::

    >>> CostTemplate = Model.get('sale.cost.template')
    >>> template_cases = CostTemplate()
    >>> template_cases.type_ = type_cases
    >>> template_cases.save()
    >>> template_uls = CostTemplate()
    >>> template_uls.type_ = type_uls
    >>> template_uls.save()
    >>> template_cases2 = CostTemplate()
    >>> template_cases2.type_ = type_cases2
    >>> template_cases2.save()
    >>> template_uls2 = CostTemplate()
    >>> template_uls2.type_ = type_uls2
    >>> template_uls2.save()


Create sale::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> sale_line.ul_cases_quantity = 5
    >>> sale_line.ul_quantity = 2
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.type = 'comment'
    >>> sale_line.description = 'Comment'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product2
    >>> sale_line.quantity = 3000.0
    >>> sale_line.unit = gram
    >>> sale_line.ul_cases_quantity = 5
    >>> sale_line.ul_quantity = 1


Check Cases quantity distribution::

    >>> sale.click('quote')
    >>> len(sale.costs)
    4
    >>> sale.click('distribute_costs')
    >>> cases_cost, = [c for c in sale.costs if c.type_.id == type_cases.id]
    >>> cases_cost.amount
    Decimal('150.00')
    >>> cases_cost.lines[0].amount, cases_cost.lines[1].amount
    (Decimal('100.00'), Decimal('50.00'))
    >>> uls_cost, = [c for c in sale.costs if c.type_.id == type_uls.id]
    >>> uls_cost.amount
    Decimal('60.00')
    >>> uls_cost.lines[0].amount, uls_cost.lines[1].amount
    (Decimal('40.00'), Decimal('20.00'))
    >>> cases_cost2, = [c for c in sale.costs if c.type_.id == type_cases2.id]
    >>> cases_cost2.amount
    Decimal('150.00')
    >>> cases_cost2.lines[0].amount, cases_cost2.lines[1].amount
    (Decimal('100.00'), Decimal('50.00'))
    >>> uls_cost2, = [c for c in sale.costs if c.type_.id == type_uls2.id]
    >>> uls_cost2.amount
    Decimal('40.00')
    >>> cost_line, = uls_cost2.lines
    >>> cost_line.amount
    Decimal('40.00')