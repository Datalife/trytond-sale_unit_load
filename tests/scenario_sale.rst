=============
Sale Scenario
=============

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()
    >>> now = datetime.datetime.now().replace(microsecond=0)

Install sale unit load::

    >>> config = activate_modules('sale_unit_load')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> customer2 = Party(name='Customer 2')
    >>> customer2.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Create another product::

    >>> aux_template = ProductTemplate()
    >>> aux_template.name = 'Aux. product'
    >>> aux_template.default_uom = unit
    >>> aux_template.type = 'goods'
    >>> aux_template.list_price = Decimal('0')
    >>> aux_template.cost_price = Decimal('2')
    >>> aux_template.save()
    >>> aux_product, = aux_template.products
    >>> aux_product.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Sale 5 products::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale.sale_date = today - relativedelta(days=1)
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 200.0
    >>> sale_line.ul_quantity = 4
    >>> sale_line.ul_cases_quantity = Decimal('20')
    >>> sale_line.cases_quantity
    80.0
    >>> sale_line.quantity_per_ul
    50.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.type = 'comment'
    >>> sale_line.description = 'Comment'
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> len(sale.shipments)
    1

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])

Create an unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.start_date = now - relativedelta(days=2)
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=5)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.quantity = Decimal('30.0')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = storage_loc
    >>> input_move = unit_load.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> output_move = unit_load.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = storage_loc
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load.save()
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> unit_load.sale_line != None
    False

Create an Inventory::

    >>> Inventory = Model.get('stock.inventory')
    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([
    ...         ('code', '=', 'STO'),
    ...         ])
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new(product=aux_product)
    >>> inventory_line.quantity = 100.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory.click('confirm')

Add ul to customer shipment::

    >>> shipment, = sale.shipments
    >>> shipment.click('draft')
    >>> shipment.start_date = now - relativedelta(days=1)
    >>> shipment.end_date = shipment.start_date + relativedelta(minutes=15)
    >>> shipment.unit_loads.append(unit_load)
    >>> shipment.save()
    >>> len(shipment.outgoing_moves)
    2
    >>> move, = [m for m in shipment.outgoing_moves
    ...     if m.product == unit_load.product]
    >>> move.origin = sale.lines[0]
    >>> move.save()
    >>> shipment.click('wait')
    >>> shipment.click('assign_try')
    True
    >>> unit_load.reload()
    >>> unit_load.sale_line == sale.lines[0]
    True
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='16bf40d731bbff6c5c1722a2ea46a165.done',
    ...     always=True).save()
    >>> shipment.click('pick')
    >>> shipment.click('pack')
    >>> unit_load.reload()
    >>> unit_load.sale_line == sale.lines[0]
    True
    >>> shipment.click('done')

Return UL with a sale::

    >>> unit_load = UnitLoad(unit_load.id)
    >>> sale_line = SaleLine(sale.lines[0].id)
    >>> do_return = Wizard('sale.return_sale', [sale])
    >>> do_return.form.new_customer = customer2
    >>> len(do_return.form.unit_loads)
    0
    >>> len(do_return.form.lines)
    2
    >>> sum(l.return_ul_quantity for l in do_return.form.lines)
    0.0
    >>> do_return.form.unit_loads.append(unit_load)
    >>> sum(l.return_ul_quantity for l in do_return.form.lines)
    1.0
    >>> do_return.execute('return_')
    >>> return_sale, = Sale.find([('id', '!=', sale.id), ('party', '=', customer.id)])
    >>> return_line, = return_sale.lines
    >>> return_line.ul_quantity
    -1.0
    >>> return_line.cases_quantity
    -5.0
    >>> return_line.quantity
    -30.0
    >>> len(return_line.unit_loads)
    1
    >>> return_line.unit_loads[0] == unit_load
    True
    >>> return_shipment, = return_sale.shipment_returns
    >>> len(return_shipment.incoming_moves)
    2
    >>> all(m.unit_load for m in return_shipment.incoming_moves)
    True
    >>> all(m.origin for m in return_shipment.incoming_moves)
    False
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='c4058c4e14a736557f9e10ad07aa6556.done',
    ...     always=True).save()
    >>> return_shipment.click('receive')
    >>> unit_load.reload()
    >>> not unit_load.sale_line
    True
    >>> unit_load.shipment == return_shipment
    True
    >>> new_sale, = Sale.find([('party', '=', customer2.id)])
    >>> new_line, = new_sale.lines
    >>> new_line.ul_quantity
    1.0
    >>> new_line.cases_quantity
    5.0

Check sale per product report::

    >>> ProductReport = Model.get('sale.reporting.product')
    >>> _ = ProductReport.find([])