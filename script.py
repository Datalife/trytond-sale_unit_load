from functools import wraps


def HaveAtr(f):

    class Atr(object):
        __name__ = 'ATR'

        atr_atr: int

    @wraps(f)
    def wrapper(*args, **kwargs):
        return f(*args, **kwargs)
    # Override signature

    if hasattr(wrapper, 'atr'):
        return Atr()
    else:
        return object()


class A(object):
    __name__ = 'A'

    atr: int


a = A()
print(a.atr)


@HaveAtr
class A():
    __name__ = 'A'

    pass


a = A()

print(a.atr_atr)
