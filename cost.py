# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval
from trytond.modules.stock_unit_load import cases_digits
from decimal import Decimal


class CostMixin(object):

    @classmethod
    def __setup__(cls):
        super(CostMixin, cls).__setup__()
        cls.distribution_method.selection.extend([
            ('cases_quantity', 'Cases quantity'),
            ('ul_quantity', 'ULs quantity')])

    def get_context_formula(self, cost):
        res = super(CostMixin, self).get_context_formula(cost)
        res['names'].update({
            'cases_quantity': Decimal(
                cost and cost.document_cases_quantity or 0),
            'ul_quantity': Decimal(
                cost and cost.document_ul_quantity or 0)})
        return res


class CostType(CostMixin, metaclass=PoolMeta):
    __name__ = 'sale.cost.type'


class CostTemplate(CostMixin, metaclass=PoolMeta):
    __name__ = 'sale.cost.template'


class CostSale(CostMixin, metaclass=PoolMeta):
    __name__ = 'sale.cost'

    document_ul_quantity = fields.Function(
        fields.Float('ULs', digits=(16, 0)),
        'get_document_ul_quantity')
    document_cases_quantity = fields.Function(
        fields.Float('Cases', digits=cases_digits),
        'get_document_cases_quantity')

    @classmethod
    def get_document_ul_quantity(cls, records, name=None):
        values = {r.id: 0 for r in records}
        document_lines = {}
        for record in records:
            document_lines.setdefault(record, []).extend(
                record._get_sale_lines())
        for record, lines in document_lines.items():
            values[record.id] = sum(line.ul_quantity or 0 for line in lines)
        return values

    @classmethod
    def get_document_cases_quantity(cls, records, name=None):
        values = {r.id: 0 for r in records}
        document_lines = {}
        for record in records:
            document_lines.setdefault(record, []).extend(
                record._get_sale_lines())
        for record, lines in document_lines.items():
            values[record.id] = sum(l.cases_quantity or 0 for l in lines)
        return values

    @fields.depends(methods=['get_document_cases_quantity',
        'get_document_ul_quantity'])
    def _compute_document_values(self):
        super()._compute_document_values()
        self.document_cases_quantity = self.get_document_cases_quantity(
            [self])[self.id]
        self.document_ul_quantity = self.get_document_ul_quantity(
            [self])[self.id]


class CostLineSale(metaclass=PoolMeta):
    __name__ = 'sale.cost.line'

    document_ul_quantity = fields.Function(
        fields.Float('ULs', digits=(16, 0)),
        'get_document_ul_quantity')
    document_cases_quantity = fields.Function(
        fields.Float('Cases', digits=cases_digits),
        'get_document_cases_quantity')

    def _compute_document_values(self):
        super()._compute_document_values()
        self.document_cases_quantity = self.get_document_cases_quantity(
            [self])[self.id]
        self.document_ul_quantity = self.get_document_ul_quantity(
            [self])[self.id]

    @classmethod
    def get_document_ul_quantity(cls, records, name=None):
        return {r.id: r.document_line.ul_quantity for r in records}

    @classmethod
    def get_document_cases_quantity(cls, records, name=None):
        return {r.id: r.document_line.cases_quantity for r in records}
